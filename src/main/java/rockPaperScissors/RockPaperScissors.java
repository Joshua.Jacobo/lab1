package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.random.*;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        System.out.println("Let's play round "+ roundCounter);
        User();
        Computermove();
        
    }


    public void User(){

        String userChoice= readInput("Your choice rock/paper/scissors");
      
        if  (userChoice !="rock" && userChoice != "paper" && userChoice != "scissors"){
            System.out.println("I dont understand "+ userChoice+ ". Could you try again?" );
    }   else if (userChoice=="rock" && userChoice=="paper" && userChoice=="scissors");
        System.out.println("good answer");
        
    }
        

    public void Computermove(){
        String randmove;
        Double randNum = Math.random()*3; 
        int computerChoice = randNum.intValue();
        if(computerChoice==1){
            randmove= "Rock";
        }
        else if (computerChoice==2){
            randmove="Scissors";
        }else{
            randmove="Paper";
            return Computermove;
        }
        }
    

   



    

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
